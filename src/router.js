import React from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';

import LoginForm from './modules/loginForm';
import EmployeeList from './modules/employeeList';
import CreateEmployeeForm from './modules/createEmployeeForm';

const RouterComponent = () => {
    return (
        <Router>
            <Stack key="root" hideNavBar>
                <Scene key="auth">
                    <Scene
                        key="loginForm"
                        component={LoginForm}
                        title="Login"
                    />
                </Scene>
                <Scene key="main">
                    <Scene
                        key="employeeList"
                        component={EmployeeList}
                        title="Employees"
                    />
                    <Scene
                        key="createEmployeeForm"
                        component={CreateEmployeeForm}
                        title="Employee"
                    />
                </Scene>
            </Stack>
        </Router>
    );
};

export default RouterComponent;
