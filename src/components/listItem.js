import React from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';

const ListItem = (props) => {
    const { viewStyle, textStyle } = styles;
    const { employee, onPress } = props;

    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={viewStyle}>
                <Text style={textStyle}>{employee.name}</Text>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ListItem;

const styles = {
    viewStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        marginBottom: 5,
        paddingBottom: 5,
        borderWidth: 1,
        borderColor: 'lightgrey',
        borderRadius: 3,
        backgroundColor: '#ffffff'
    },
    textStyle: {
        fontSize: 16,
        textTransform: 'capitalize',
    },
};
