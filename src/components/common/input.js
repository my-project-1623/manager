import React from 'react';
import { Text, View, TextInput } from 'react-native';

const Input = (props) => {
    const {
        containerStyle,
        textStyle,
        inputStyle,
        textContainerStyle
    } = styles;
    const {
        label,
        value,
        placeholder,
        autoCorrect,
        secureTextEntry,
        onChangeText
    } = props;

    return (
        <View style={containerStyle}>
            {
                label &&
                <View style={textContainerStyle}>
                    <Text style={textStyle}>{label}</Text>
                </View>
            }
            <TextInput
                secureTextEntry={secureTextEntry}
                autoCorrect={autoCorrect}
                placeholder={placeholder}
                style={inputStyle}
                value={value}
                onChangeText={onChangeText}
            />
        </View>
    );
};

export { Input };

const styles = {
    containerStyle: {
        display: 'flex',
        flexDirection: 'row',
        padding: 5,
    },
    textContainerStyle: {
        justifyContent: 'center',
        paddingLeft: 5,
        paddingRight: 5,
        flex: 1
    },
    textStyle: {
        fontSize: 16,
        textTransform: 'capitalize',
    },
    inputStyle: {
        height: 40,
        flex: 3,
        fontSize: 16,
        paddingLeft: 5,
        paddingRight: 10,
    }
};
