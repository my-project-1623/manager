import React from 'react';
import { View } from 'react-native';

const Card = ({ children, style }) => {
    const { viewStyle } = styles;
    return (
        <View style={[viewStyle, style]}>
            {children}
        </View>
    );
};

export { Card };

const styles = {
    viewStyle: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        borderRadius: 3,
        backgroundColor: '#fff',
        padding: 5,
    },
};
