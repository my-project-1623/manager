import React from 'react';
import { Text, TouchableOpacity, ActivityIndicator } from 'react-native';

const Button = (props) => {
    const { buttonContainerStyle, buttonTextStyle } = styles;
    const { onPress, text, loading, buttonStyle } = props;
    return (
        <TouchableOpacity style={{ ...buttonContainerStyle, ...buttonStyle }} onPress={onPress}>
            {
                loading &&
                <ActivityIndicator color="#007aff" size="small" />
            }
            <Text style={buttonTextStyle}>{text}</Text>
        </TouchableOpacity>
    );
};

export { Button };

const styles = {
    buttonContainerStyle: {
        padding: 10,
        borderColor: '#007aff',
        borderWidth: 1,
        borderRadius: 3,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    buttonTextStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        textAlign: 'center',
        color: '#007aff',
        fontWeight: '600',
        fontSize: 16,
    }
};
