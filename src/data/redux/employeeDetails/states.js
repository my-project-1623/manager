const states = {
    employeeDetails: {
        name: '',
        phone: '',
        shift: 'wednesday',
        loading: false,
        employeeListFetching: false,
        employeeList: {}
    }
};

export default states;
