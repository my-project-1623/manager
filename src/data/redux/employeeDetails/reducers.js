import actionTypes from '../actionTypes';
import states from './states';

const employeeDetails = (state = states.employeeDetails, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_EMPLOYEE_STATE:
            return {
                ...state,
                [action.payload.name]: action.payload.value,
            };

        case actionTypes.EMPLOYEE_CREATING:
            return {
                ...state,
                loading: true,
            };

        case actionTypes.EMPLOYEE_CREATED:
            return {
                ...state,
                name: '',
                phone: '',
                loading: false,
            };

        case actionTypes.EMPLOYEE_CREATION_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case actionTypes.EMPLOYEE_LIST_FETCHING:
            return {
                ...state,
                employeeListFetching: true,
            };

        case actionTypes.EMPLOYEE_LIST_FETCHED:
            console.log(action.payload);
            return {
                ...state,
                employeeListFetching: false,
                employeeList: action.payload,
            };

        default:
            return state;
    }
};

export default employeeDetails;

