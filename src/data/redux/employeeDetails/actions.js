import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import actionTypes from '../actionTypes';

export const updateEmployeeState = ({ name, value }) => {
    return {
        type: actionTypes.UPDATE_EMPLOYEE_STATE,
        payload: { name, value }
    };
};

export const fetchEmployeeList = () => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        dispatch({
            type: actionTypes.EMPLOYEE_LIST_FETCHING
        });
        firebase.database().ref(`users/${currentUser.uid}/employees`)
            .on('value', (snapshot) => { //this is socket listener
                console.log('data fetched', snapshot);
                dispatch({
                    type: actionTypes.EMPLOYEE_LIST_FETCHED,
                    payload: snapshot.val()
                });
            });
    };
};

const employeeCreationSuccess = (dispatch) => {
    dispatch({
        type: actionTypes.EMPLOYEE_CREATED
    });
    Actions.employeeList();
};

const employeeCreationFailed = (dispatch, error) => {
    dispatch({
        type: actionTypes.EMPLOYEE_CREATION_ERROR,
        payload: error.message ? error.message : 'some error occured.'
    });
};

export const createEmployee = ({ name, phone, shift }) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        dispatch({
            type: actionTypes.EMPLOYEE_CREATING
        });

        firebase.database().ref(`users/${currentUser.uid}/employees`)
            .push({ name, phone, shift })
            .then(() => employeeCreationSuccess(dispatch))
            .catch(error => employeeCreationFailed(dispatch, error));
    };
};

const employeeUpdationSuccess = (dispatch) => {
    dispatch({
        type: actionTypes.EMPLOYEE_UPDATED
    });
    Actions.employeeList();
};

const employeeUpdationFailed = (dispatch, error) => {
    dispatch({
        type: actionTypes.EMPLOYEE_UPDATION_ERROR,
        payload: error.message ? error.message : 'some error occured.'
    });
};

export const updateEmployee = ({ name, phone, shift, eid }) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        dispatch({
            type: actionTypes.EMPLOYEE_UPDATING
        });

        firebase.database().ref(`users/${currentUser.uid}/employees/${eid}`)
            .set({ name, phone, shift })
            .then(() => employeeUpdationSuccess(dispatch))
            .catch(error => employeeUpdationFailed(dispatch, error));
    };
};

const employeeDeletionSuccess = (dispatch) => {
    dispatch({
        type: actionTypes.EMPLOYEE_DELETED
    });
    Actions.employeeList();
};

const employeeDeletionFailed = (dispatch, error) => {
    dispatch({
        type: actionTypes.EMPLOYEE_DELETION_ERROR,
        payload: error.message ? error.message : 'some error occured.'
    });
};

export const deleteEmployee = ({ eid }) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        dispatch({
            type: actionTypes.EMPLOYEE_DELETING
        });

        firebase.database().ref(`users/${currentUser.uid}/employees/${eid}`)
            .remove()
            .then(() => employeeDeletionSuccess(dispatch))
            .catch(error => employeeDeletionFailed(dispatch, error));
    };
};
