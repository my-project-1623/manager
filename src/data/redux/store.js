import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import rootReducer from './rootReducer';

const createReduxStore = () => {
    const store = createStore(
        rootReducer,
        {},
        applyMiddleware(ReduxThunk)
    );
    return store;
};

export default createReduxStore;

