const states = {
    authDetails: {
        email: '',
        password: '',
        user: null,
        error: '',
        logging: false,
    }
};

export default states;
