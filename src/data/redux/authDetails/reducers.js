import actionTypes from '../actionTypes';
import states from './states';

const authDetails = (state = states.authDetails, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_AUTH_STATE:
            return {
                ...state,
                [action.payload.name]: action.payload.value,
            };

        case actionTypes.USER_LOGGING:
            return {
                ...state,
                logging: true,
                error: ''
            };

        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                logging: false,
                email: '',
                password: '',
                error: '',
                user: action.payload,
            };

        case actionTypes.LOGIN_FAILED:
            return {
                ...state,
                logging: false,
                password: '',
                user: null,
                error: action.payload
            };

        default:
            return state;
    }
};

export default authDetails;

