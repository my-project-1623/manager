import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import actionTypes from '../actionTypes';

export const updateAuthState = ({ name, value }) => {
    return {
        type: actionTypes.UPDATE_AUTH_STATE,
        payload: { name, value }
    };
};

const loginSuccess = (dispatch, user) => {
    console.log(user);
    dispatch({
        type: actionTypes.LOGIN_SUCCESS,
        payload: user
    });
    Actions.main();
};

const LoginFailed = (dispatch, error) => {
    console.log('login Failed error', error);
    dispatch({
        type: actionTypes.LOGIN_FAILED,
        payload: error.message ? error.message : 'Authentication Failed'
    });
};

export const loginUser = ({ email, password }) => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.USER_LOGGING
        });
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((user) => loginSuccess(dispatch, user))
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then((user) => loginSuccess(dispatch, user))
                    .catch((error) => LoginFailed(dispatch, error));
            });
    };
};
