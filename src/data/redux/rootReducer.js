import { combineReducers } from 'redux';

import authDetails from './authDetails/reducers';
import employeeDetails from './employeeDetails/reducers';

const rootReducer = combineReducers({
    authDetails,
    employeeDetails
});

export default rootReducer;
