import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Text, View } from 'react-native';

import * as authActions from '../data/redux/authDetails/actions';

import { Card, Input, Button } from '../components/common';

class LoginForm extends Component {
    renderErrorMessage = () => {
        const { error } = this.props.authDetails;
        if (error.trim().length > 0) {
            return (
                <Text style={{ fontSize: 13, color: 'red', textAlign: 'center' }}>{error}</Text>
            );
        }
        return null;
    };

    render() {
        const { actions } = this.props;
        const { email, password, logging } = this.props.authDetails;
        return (
            <View style={{ flex: 1, padding: 10 }}>
                <Card>
                    <Input
                        label='Email'
                        placeholder='user@gmail.com'
                        value={email}
                        onChangeText={(value) => actions.updateAuthState({ name: 'email', value })}
                    />
                    <Input
                        secureTextEntry
                        label='Password'
                        placeholder='password'
                        value={password}
                        onChangeText={(value) => actions.updateAuthState({ name: 'password', value })}
                    />
                    {this.renderErrorMessage()}
                    <Button
                        loading={logging}
                        text={logging ? 'logging' : 'Login'}
                        buttonStyle={{ marginTop: 10 }}
                        onPress={() => actions.loginUser({ email, password })}
                    />
                </Card>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        authDetails: state.authDetails
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, authActions), dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);

