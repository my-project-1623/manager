import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActivityIndicator, View, VirtualizedList } from 'react-native';

import * as employeeActions from '../data/redux/employeeDetails/actions';

import ListItem from '../components/listItem';

class EmployeeList extends Component {
    componentDidMount() {
        this.props.actions.fetchEmployeeList();
    }

    renderListItem = ({ item }) => {
        console.log('item', item);
        return (
            <ListItem employee={item} />
        );
    }

    render() {
        const { employeeListFetching, employeeList } = this.props.employeeDetails;
        if (employeeListFetching) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" />
                </View>
            );
        }

        return (
            <View style={{ padding: 10 }}>
                <VirtualizedList
                    data={employeeList}
                    getItem={(data, index) => { return data[Object.keys(data)[index]]; }}
                    getItemCount={data => { return Object.keys(data).length; }}
                    keyExtractor={(data, index) => { return Object.keys(data)[index]; }}
                    renderItem={this.renderListItem}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        employeeDetails: state.employeeDetails
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, employeeActions), dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeList);
