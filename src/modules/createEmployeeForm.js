import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, Picker } from 'react-native';

import * as employeeActions from '../data/redux/employeeDetails/actions';
import { Card, Input, Button } from '../components/common';

class CreateEmployeeForm extends Component {
    render() {
        const { loading, name, phone, shift } = this.props.employeeDetails;
        const { actions } = this.props;
        return (
            <View style={{ flex: 1, padding: 10 }}>
                <Card>
                    <Input
                        label='Name'
                        placeholder='vivek'
                        value={name}
                        onChangeText={(value) => actions.updateEmployeeState({
                            name: 'name', value
                        })}
                    />
                    <Input
                        label='Phone'
                        placeholder='9999995432'
                        value={phone}
                        onChangeText={(value) => actions.updateEmployeeState({
                            name: 'phone', value
                        })}
                    />
                    <Text style={styles.labelStyle}>Shift</Text>
                    <Picker
                        selectedValue={shift}
                        onValueChange={(value) => actions.updateEmployeeState({
                            name: 'shift', value
                        })}
                    >
                        <Picker.Item label="Monday" value="monday" />
                        <Picker.Item label="Tuesday" value="tuesday" />
                        <Picker.Item label="Wednesday" value="wednesday" />
                        <Picker.Item label="Thersday" value="thersady" />
                        <Picker.Item label="Friday" value="friday" />
                        <Picker.Item label="Saturday" value="saturday" />
                        <Picker.Item label="Sunday" value="sunday" />
                    </Picker>
                    <Button
                        loading={loading}
                        text={loading ? 'Creating' : 'Create'}
                        buttonStyle={{ marginTop: 10 }}
                        onPress={() => actions.createEmployee({ 
                            name, phone, shift 
                        })}
                    />
                </Card>
            </View>
        );
    }
}

const styles = {
    labelStyle: {
        fontSize: 16,
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
    }
};

function mapStateToProps(state) {
    return {
        employeeDetails: state.employeeDetails
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, employeeActions), dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateEmployeeForm);
