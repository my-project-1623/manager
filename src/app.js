import React, { Component } from 'react';
import { Provider } from 'react-redux';
import firebase from 'firebase';

import createReduxStore from './data/redux/store';
import Router from './router';

class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyDoEz15XL4BtE0m3JRPD-MdJ05Mxcbs0pc',
            authDomain: 'manager-1623.firebaseapp.com',
            databaseURL: 'https://manager-1623.firebaseio.com',
            projectId: 'manager-1623',
            storageBucket: 'manager-1623.appspot.com',
            messagingSenderId: '798410195709'
        };
        firebase.initializeApp(config);
    }

    render() {
        const store = createReduxStore();
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
